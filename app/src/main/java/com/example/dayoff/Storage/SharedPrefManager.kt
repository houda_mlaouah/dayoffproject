package com.example.dayoff.Storage

import android.annotation.SuppressLint
import android.content.Context
import com.example.dayoff.models.User
class SharedPrefManager private constructor(private val mCtx: Context) {

        val isLoggedIn: String?
            get() {
                val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
                return sharedPreferences.getString("_id", (-1).toString())

            }

        val user: User
            get() {
                val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
                return User(
                    sharedPreferences.getString("_id", (-1).toString())!!,
                    sharedPreferences.getString("email", null)!!,
                    sharedPreferences.getString("status", null)!! ,
                    sharedPreferences.getString("name", null)!!

                )
            }


        fun saveUser(user: User) {

            val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()

            editor.putString("id", user._id)
            editor.putString("email", user.email)
            editor.putString("name", user.name)
            editor.putString("status", user.status)

            editor.apply()

        }

        fun clear() {
            val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.clear()
            editor.apply()
        }

        companion object {
            private val SHARED_PREF_NAME = "my_shared_preff"
            @SuppressLint("StaticFieldLeak")
            private var mInstance: SharedPrefManager? = null
            @Synchronized
            fun getInstance(mCtx: Context): SharedPrefManager {
                if (mInstance == null) {
                    mInstance = SharedPrefManager(mCtx)
                }
                return mInstance as SharedPrefManager
            }
        }

}