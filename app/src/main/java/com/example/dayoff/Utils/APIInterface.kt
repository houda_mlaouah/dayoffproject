package com.example.dayoff.Utils

import com.example.dayoff.models.*
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import io.reactivex.Observable
import retrofit2.Call

interface APIInterface {
    @POST("signup")
    fun RegisterEmploye(@Body signUpRequest: signUpRequest
                            ):Call<LoginResponse>


    @POST("login")
    fun LogInEmploye(@Body loginRequest: LoginRequest
                        ):Call<LoginResponse>

}