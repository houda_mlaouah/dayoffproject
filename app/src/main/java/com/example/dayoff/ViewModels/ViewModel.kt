package com.example.dayoff.ViewModels

import android.annotation.SuppressLint
import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.dayoff.Repositories.Repository
import com.example.dayoff.models.EmployeEntity
import com.example.dayoff.room.App
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job


class ViewModel(application: Application):AndroidViewModel(application) {

    private val repository: Repository
    @SuppressLint("StaticFieldLeak")
    private val context = getApplication<Application>().applicationContext

    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    val status:String="employee"
    init {
        //val employeDao = App.database.employeDao()

        repository = Repository(/*employeDao ,*/ context)
    }
   /* fun addEmp(employeEntity: EmployeEntity){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addEmp(employeEntity)
        }
    }*/

   /* fun addEmploye( employeEntity: EmployeEntity){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addEmployee(employeEntity)
        }
    }*/


    /*fun loginEmployee(email: String, password: String) {
            uiScope.launch {
                val usersNames = repository.getEmp(email)
                    Log.i("user", "$usersNames")
                if (usersNames.status == "employee"){
                    if (usersNames != null) {
                        if(usersNames.password == password){
                            Toast.makeText(context, "welcome back , employee" ,Toast.LENGTH_LONG).show()

                        }else{
                            Toast.makeText(context, "wrong password" ,Toast.LENGTH_LONG).show()
                        }
                    } else {
                        Toast.makeText(context, "please fill all fields" ,Toast.LENGTH_LONG).show()
                    }
                }
            }
    }
    fun loginManager(email: String, password: String) {
        uiScope.launch {
            val usersNames = repository.getEmp(email)
            Log.i("user", "$usersNames")
            if (usersNames.status == "manager"){
                if (usersNames != null) {
                    if(usersNames.password == password){
                        Toast.makeText(context, "welcome back , manager" ,Toast.LENGTH_LONG).show()

                    }else{
                        Toast.makeText(context, "wrong password" ,Toast.LENGTH_LONG).show()
                    }
                } else {
                    Toast.makeText(context, "please fill all fields" ,Toast.LENGTH_LONG).show()
                }
            }
        }
    }*/
}