package com.example.dayoff.models

data class LoginRequest(
    val email : String,
    val password : String
)
