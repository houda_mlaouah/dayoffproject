package com.example.dayoff.models

import java.io.Serializable

data class Data (
    val user: User
        ):Serializable