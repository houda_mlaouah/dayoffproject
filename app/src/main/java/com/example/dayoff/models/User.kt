package com.example.dayoff.models

import java.io.Serializable

data class User (
    val status :String,
    val _id:String,
    val name:String,
    val email :String,
    ):Serializable