package com.example.dayoff.models

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "Employe-table")
data class EmployeEntity(
    @PrimaryKey(autoGenerate = true)
    val id:Int=0 ,
    val name:String="",
    val email :String="",
    val password:String="",
    val status :String=""


)