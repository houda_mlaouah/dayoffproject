package com.example.dayoff.models

data class DefautResponse(
    val status: String,
    val message:String
)
