package com.example.dayoff.models

import java.io.Serializable

data class LoginResponse (
    val status: String,
    val token:String,
    val data:Data
    ):Serializable