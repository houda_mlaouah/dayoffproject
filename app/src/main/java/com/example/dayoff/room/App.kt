package com.example.dayoff.room

import android.app.Application
import androidx.room.Room


class App: Application() {
    companion object{
        lateinit var database: EmployeeDataBase
    }
    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(
            this,
            EmployeeDataBase::class.java,
            "databaseName"
        ).fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
    }

}