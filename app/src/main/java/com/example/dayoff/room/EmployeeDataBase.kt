package com.example.dayoff.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.dayoff.models.EmployeEntity

@Database(entities = [ EmployeEntity::class], version = 4)
abstract class EmployeeDataBase:RoomDatabase() {


    abstract fun employeDao():EmpDao

}