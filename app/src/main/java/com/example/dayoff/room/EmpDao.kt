package com.example.dayoff.room

import androidx.room.*
import com.example.dayoff.models.EmployeEntity
import kotlinx.coroutines.flow.Flow
@Dao
interface EmpDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(employeEntity: EmployeEntity)
    @Update
    suspend fun update(employeEntity: EmployeEntity)

    @Delete
    suspend fun delete(employeEntity: EmployeEntity)

    @Query("SELECT* FROM `Employe-table`")
    //Flow notify de rv that the data has been changed
    fun fetchAllEmployees(): Flow<List<EmployeEntity>>

    @Query("SELECT* FROM `Employe-table` where id=:id")
    fun fetchEmployeById(id:Int): Flow<EmployeEntity>

    @Query("SELECT * FROM `Employe-table`  where email  LIKE :email")
    fun GetEmployeByEmail(email:String): EmployeEntity
}