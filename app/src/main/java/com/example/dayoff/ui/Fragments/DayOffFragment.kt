package com.example.dayoff.ui.Fragments

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import com.example.dayoff.R
import com.example.dayoff.databinding.FragmentDayOffBinding
import java.util.*


class DayOffFragment : Fragment(R.layout.fragment_day_off) {
    private var binding: FragmentDayOffBinding? = null
    lateinit var startDate:String
    lateinit var endDate:String
    lateinit var theDate:Date
    lateinit var theDatee:Date
    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding = FragmentDayOffBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)

        binding!!.sendReqBtn.setOnClickListener {
            Toast.makeText(context, "request send, you'll be notified soon", Toast.LENGTH_LONG).show()
        }
        //calendar
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        binding!!.startDateLl.setOnClickListener {
            val dpd = DatePickerDialog(requireContext(),DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                //set to text view
               startDate = binding!!.startDate.setText("" + dayOfMonth + "/" + monthOfYear + "/" + year).toString()
           //     val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
           //             theDatee = sdf.parse(startDate)
            },
            year, month, day)
            //show dialog
            dpd.show()
        }
        binding!!.endDateLl.setOnClickListener {
            val dpd = DatePickerDialog(requireContext(),DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                //set to text view
               endDate =   binding!!.endDate.setText(""+dayOfMonth+"/"+monthOfYear+"/"+year).toString()
              //  val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
               // theDate = sdf.parse(endDate)

            },
                year, month, day)
            //show dialog
            dpd.show()
        }
        // val daysOff = (theDatee.time - theDate.time).days
       //Log.i("dd","$daysOff")
       //binding!!.dayOffNumber.setText("Day off request is for ${days} day")


    }
    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}