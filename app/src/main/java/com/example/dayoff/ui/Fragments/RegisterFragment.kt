package com.example.dayoff.ui.Fragments

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.dayoff.R
import com.example.dayoff.Utils.APIClient
import com.example.dayoff.Utils.APIInterface
import com.example.dayoff.ViewModels.ViewModel
import com.example.dayoff.databinding.FragmentRegisterBinding
import com.example.dayoff.models.*
import com.example.dayoff.room.App
import com.example.dayoff.room.EmpDao
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.*
import java.lang.Exception


class RegisterFragment : Fragment(R.layout.fragment_register){
    private var binding: FragmentRegisterBinding? = null
    private val managerViewModel:ViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding = FragmentRegisterBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)
        binding!!.btnLogRegister.setOnClickListener {
            findNavController().navigate(R.id.action_registerFragment_to_authFragment)
        }

        binding!!.buttonRegister.setOnClickListener {

            val signUpRequest =signUpRequest(binding!!.etName.text.toString(),binding!!.etEmail.text.toString(),binding!!.etPassword.text.toString())
            APIClient.instance.RegisterEmploye(signUpRequest)
                .enqueue(object: Callback<LoginResponse>{
                    override fun onResponse(
                        call: Call<LoginResponse>,
                        response: Response<LoginResponse>
                    ) {
                        Log.i("Tag", "$response")
                        Toast.makeText(context,"hello",Toast.LENGTH_LONG).show()
                    }
                    override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                        Toast.makeText(context,t.message,Toast.LENGTH_LONG).show()
                    }

                })
        }
    }




    //add new Manager

    /*fun addManager(EmpDao: EmpDao){
        val name = binding!!.etName.text.toString()
        val email = binding!!.etEmail.text.toString()
        val password = binding!!.etPassword.text.toString()

        if (inputCheck(name,email,password)){
            //create manager object
            val manager = EmployeEntity(0, name, email , password,"manager")
            // Add data to database
            managerViewModel.addEmp(manager)
            Toast.makeText(context,
                "added successfully",
                Toast.LENGTH_LONG
            ).show()
            findNavController().navigate(R.id.action_registerFragment_to_authFragment)
        }else{
            Toast.makeText(context,
                "Email or password cannot be blank",
                Toast.LENGTH_LONG
            ).show()

        }
    }*/
    /*fun addEmployee(){
        val name = binding!!.etName.text.toString()
        val email = binding!!.etEmail.text.toString()
        val password = binding!!.etPassword.text.toString()
        if (inputCheck(name,email,password)){
            try {
                val employee = EmployeEntity(0,name , email,password)
                managerViewModel.addEmploye(employee)
            }catch (e : Exception){
                e.printStackTrace()
            }
        }else{
            Toast.makeText(context,
                "Email or password cannot be blank",
                Toast.LENGTH_LONG
            ).show()
        }
    }*/
    // check if the inputs are not empty
    private fun inputCheck(name:String, email:String, password:String):Boolean{
        return !(TextUtils.isEmpty(name) && TextUtils.isEmpty(email) &&TextUtils.isEmpty(password) )
    }


}