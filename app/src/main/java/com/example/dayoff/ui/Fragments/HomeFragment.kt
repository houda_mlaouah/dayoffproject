package com.example.dayoff.ui.Fragments

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.example.dayoff.R
import com.example.dayoff.databinding.FragmentHomeBinding
import java.util.*
class HomeFragment : Fragment(R.layout.fragment_home) {
    private var binding: FragmentHomeBinding? = null
    lateinit var Date: String
    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding = FragmentHomeBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)
        binding!!.calendarView.setOnDateChangeListener { calendarView, i, i2, i3 ->
            Toast.makeText(context,"select Date:$i3/$i2/$i", Toast.LENGTH_LONG).show()
            //val sdf = SimpleDateFormat("yyyy/MM/dd")
            Date = "$i3/$i2/$i"
            Log.i("Date", Date)
            binding!!.DateTv.text = Date
        }
        /*val  localDate=LocalDate.now()
       binding!!.DateTv.text = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(localDate)*/
    }
    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

}