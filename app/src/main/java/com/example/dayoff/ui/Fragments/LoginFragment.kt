package com.example.dayoff.ui.Fragments

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.dayoff.AuthActivity
import com.example.dayoff.MainActivity
import com.example.dayoff.R
import com.example.dayoff.Utils.APIClient
import com.example.dayoff.Utils.APIInterface
import com.example.dayoff.ViewModels.ViewModel
import com.example.dayoff.databinding.FragmentLoginBinding
import com.example.dayoff.models.LoginRequest
import com.example.dayoff.models.LoginResponse
import com.example.dayoff.room.App
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginFragment :  Fragment(R.layout.fragment_login){
    private var binding: FragmentLoginBinding? = null
    private val managerViewModel: ViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding = FragmentLoginBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)
        binding!!.btnRegLogin.setOnClickListener {
            findNavController().navigate(R.id.action_authFragment_to_registerFragment)
        }

        binding!!.button.setOnClickListener {
            val email=binding!!.etEmail.text.toString()
            val password=binding!!.etPassword.text.toString()
            val loginRequest = LoginRequest(email ,password)
            if(inputCheck(email,password)){
                APIClient.instance.LogInEmploye(loginRequest)
                    .enqueue(object: Callback<LoginResponse> {
                        override fun onResponse(
                            call: Call<LoginResponse>,
                            response: Response<LoginResponse>
                        ) {
                            Log.i("TagLogin", "${response.body()}")
                            if (response.body()?.status == "success"){
                                Toast.makeText(context,"hello",Toast.LENGTH_LONG).show()
                                startActivity(Intent(context,MainActivity::class.java))
                            }else{
                                Toast.makeText(context,response.body()?.status.toString(),Toast.LENGTH_LONG).show()

                            }


                        }
                        override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                            Toast.makeText(context,t.message,Toast.LENGTH_LONG).show()
                        }
                    }
                    )
            }else{
                Toast.makeText(context, "please fill all fields" ,Toast.LENGTH_LONG).show()

            }

        }
    }



    /*private fun loginemployee(){
        val email = binding!!.etEmail.text.toString()
        val password = binding!!.etPassword.text.toString()
        if(inputCheck(email,password)){
            managerViewModel.loginEmployee(email, password)
            startActivity(Intent(context,AuthActivity::class.java))

        }
    }
    private fun loginmanager(){
        val email = binding!!.etEmail.text.toString()
        val password = binding!!.etPassword.text.toString()
        if(inputCheck(email,password)){
            managerViewModel.loginManager(email, password)
            startActivity(Intent(context,MainActivity::class.java))

        }
    }*/

    private fun inputCheck(email:String, password:String):Boolean{
        return !(TextUtils.isEmpty(email) && TextUtils.isEmpty(password) )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

}