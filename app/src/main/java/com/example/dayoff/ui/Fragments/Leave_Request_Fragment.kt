package com.example.dayoff.ui.Fragments

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.dayoff.R
import com.example.dayoff.ViewModels.ViewModel
import com.example.dayoff.databinding.FragmentLeaveRequestBinding
import com.example.dayoff.models.EmployeEntity
import com.example.dayoff.room.App
import com.example.dayoff.room.EmpDao


class Leave_Request_Fragment : Fragment(R.layout.fragment_leave__request_)  {
    private var binding: FragmentLeaveRequestBinding? = null
    private val ViewModel: ViewModel by viewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding = FragmentLeaveRequestBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)
        val EmpDao = App.database.employeDao()

        binding!!.buttonRegister2.setOnClickListener {
            addEmployee(EmpDao)
        }
    }
    fun addEmployee(EmpDao: EmpDao){
        val name = binding!!.etName.text.toString()
        val email = binding!!.etEmail.text.toString()
        val password = binding!!.etPassword.text.toString()

        if (inputCheck(name,email,password)){
            //create manager object
            val employee = EmployeEntity(0, name, email , password,"employee")
            // Add data to database
            //ViewModel.addEmp(employee)
            Toast.makeText(context,
                "added successfully",
                Toast.LENGTH_LONG
            ).show()
        }else{
            Toast.makeText(context,
                "Email or password cannot be blank",
                Toast.LENGTH_LONG
            ).show()

        }
    }
    // check if the inputs are not empty
    private fun inputCheck(name:String, email:String, password:String):Boolean{
        return !(TextUtils.isEmpty(name) && TextUtils.isEmpty(email) && TextUtils.isEmpty(password) )
    }

}